import AccountRepository from "@/repositories/AccountRepository";

export default {
  register,
  getToken,
};

async function register(user) {
  await AccountRepository.registerAccount(user);
  return getToken();
}

function getToken() {
  return localStorage.getItem("token");
}
