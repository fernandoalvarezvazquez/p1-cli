import HTTP from "@/common/http";

const resource = "users";
const resource2 = "posts";

// función para hacer las llamadas lentas a propósito
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export default {
  async findAll() {
    const response = await HTTP.get(resource);
    return response.data;
  },

  async findOne(id) {
    await sleep(1000);
    return (await HTTP.get(`${resource}/${id}`)).data;
  },

  async findByLogin(login) {
    await sleep(1000);
    return (await HTTP.get(`${resource}/${login}`)).data;
  },

  async findAllPosts(id) {
    await sleep(1000);
    return (await HTTP.get(`${resource}/${id}/${resource2}`)).data;
  },
};
