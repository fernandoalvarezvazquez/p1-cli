import UserList from "./UserList";
import UserForm from "./UserForm";
import UserDetail from "./UserDetail";

const routes = [
  {
    name: "UserList",
    path: "/users",
    component: UserList,
    meta: { public: true },
  },
  // /posts/new debe colocarse antes de /posts/:id porque si no vue-router
  // interpreta "new" como si fuera el id.
  //
  // Una forma de evitar este problema es usar una expresión regular para
  // limitar los valores que son interpretados. Por ejemplo, usando el path
  // /posts/:id(\\d+), vue-router espera que :id sea numérico.
  {
    name: "UserCreate",
    path: "/register",
    component: UserForm,
  },
  {
    name: "UserDetail",
    path: "/account",
    component: UserDetail,
    meta: { public: true },
  },
  {
    name: "UserUpdate",
    path: "/users/:id/update",
    component: UserForm,
  },
];

export default routes;
